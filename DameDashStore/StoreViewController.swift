//
//  StoreViewController.swift
//  DameDashStore
//
//  Created by Sanchan on 04/01/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit
import AVKit
import Kingfisher

class StoreViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    var productData = NSDictionary()
    var userId:String!
    var player: AVQueuePlayer!
    var button = UIButton()
    var productList = NSArray()
    var productCollectionList = NSMutableArray()
    var quickBuyView = UIView()
    var buyButton = UIButton()
    var playerLayer = AVPlayerLayer()
    var imageLogo = UIImageView()
    var producttitle = UILabel()
    var pricelbl = UILabel()
    var seektime = Float64()
    var i:Int = 0
    var data = NSArray()
    var playerController:AVPlayerViewController!
    var videoItemList = [AVPlayerItem]()
 
    
    
    var collectionView: UICollectionView!
    var viewToFocus: UIView? = nil {
        didSet {
            if viewToFocus != nil {
                self.setNeedsFocusUpdate();
                self.updateFocusIfNeeded();
            }
        }
    }
    override weak var preferredFocusedView: UIView? {
        if viewToFocus != nil {
            return viewToFocus;
        } else {
            return super.preferredFocusedView
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getProducts()
       /* playerController = AVPlayerViewController()
        playerController.view.frame = self.view.frame
     //   self.addChildViewController(playerController)
        self.view.addSubview(playerController.view)
        let queuePlayer = AVQueuePlayer(items: videoItemList)
        playerController.player = queuePlayer
        playerController.player?.play()*/
      //  player = AVQueuePlayer(items: videoItemList)
        player = AVQueuePlayer.init(items: videoItemList)
        var playerLayer = AVPlayerLayer.init(player: player)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer.zPosition = -1
        playerLayer.frame = CGRect(x: 0, y: 0, width: 1920, height: 1080)
        
        // Video Url

     /*   player = AVQueuePlayer(items: videoItemList)
       // player?.actionAtItemEnd = .none
        player?.isMuted = true
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer.zPosition = -1
        view.backgroundColor = UIColor.white
       // playerLayer.frame = CGRect(x: 150, y: 0, width: 1620, height: 650)
        playerLayer.frame = CGRect(x: 0, y: 0, width: 1920, height: 1080)*/
        
        //Layout
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 220, height: 193)
        layout.minimumInteritemSpacing = 30
        layout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10)
        layout.scrollDirection = .horizontal
        
        //Collection View
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 650, width: 1920, height: 430), collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.backgroundColor = UIColor.clear
        viewToFocus = collectionView
        view.addSubview(collectionView)
        
        // Qucik By View
        quickBuyView = UIView(frame: CGRect(x: 150, y: 150, width: 400, height: 400))
        quickBuyView.backgroundColor = UIColor.white
        quickBuyView.layer.cornerRadius = 7.0
        imageLogo.frame = CGRect(x: 60, y: 30, width: 250, height: 100)
        producttitle.frame = CGRect(x: 10, y: 120, width: 400, height: 50)
        producttitle.font = UIFont(name:"Helvetica-Bold", size: 25)
        pricelbl = UILabel(frame: CGRect(x: 10, y: 160, width: 200, height: 50))
        pricelbl.font = UIFont(name:"Helvetica", size: 25)
        pricelbl.textColor = UIColor.red
        buyButton = UIButton(type: .system)
        buyButton.frame = CGRect(x: 85, y: 250, width: 230, height: 50)
        buyButton.backgroundColor = UIColor.red
        buyButton.setTitle("Speed Buy", for: .normal)
        buyButton.titleLabel?.font = UIFont.systemFont(ofSize: 30)
        quickBuyView.addSubview(imageLogo)
        quickBuyView.addSubview(producttitle)
        quickBuyView.addSubview(pricelbl)
        
        quickBuyView.addSubview(buyButton)
        
        view.addSubview(quickBuyView)
        view.layer.addSublayer(playerLayer)
  
        player?.play()
        //loop video
        NotificationCenter.default.addObserver(self,selector: #selector(StoreViewController.loopVideo(sender:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,object: player?.items().last)
//        NotificationCenter.default.addObserver(self, selector:#selector(StoreViewController.buffercheck) , name: (player?.currentItem?.isPlaybackBufferEmpty), object: nil)
        player?.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 1), queue: DispatchQueue.main, using:
            {_ in
                let activityView = ActivityView.init(frame: self.view.frame)
                if self.player?.currentItem?.status == .readyToPlay
                {
                    self.seektime = CMTimeGetSeconds(self.player!.currentTime())
                    if self.seektime > 10.00
                    {
                      self.timeCheck()
                    }
                    if self.seektime > 30.0
                    {
                     //   self.removecheck()
                    }
                }
                if (self.player?.currentItem?.isPlaybackBufferEmpty)!
                {
                    print("player buffer is empty")
                   // self.view.addSubview(activityView)
                }
              //  activityView.removeFromSuperview()
        })
        quickBuyView.isHidden = true
        collectionView.isHidden = true

    }
    func pressButton()
    {
        NSLog("welcome to shopping cart", 0)
    }
    
    func timeCheck()
    {
        quickBuyView.isHidden = false
        collectionView.isHidden = false
    }
    func removecheck()
    {
        quickBuyView.isHidden = true
        collectionView.isHidden = true
    }
    
    func buffercheck()
    {
        
    }
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.items().last)
    }
    
    func loopVideo(sender:NSNotification) {
        
//        if sender.object as? AVPlayerItem == videoItemList.last
//        {
      //  player?.removeAllItems()
      /*  player?.removeAllItems()
        let item = player?.currentItem
        player?.remove(item!)
        player?.removeAllItems()
        for i in videoItemList
        {
        }
        player?.insert(videoItemList.first!, after: nil)
        player?.seek(to: kCMTimeZero)
        player?.play()*/
//        player?.advanceToNextItem()
    
//            player?.removeAllItems()
//            let loopItem = player?.items().first
//            loopItem?.seek(to: kCMTimeZero)
//            player?.insert(loopItem!, after: nil)
           // player = AVQueuePlayer(items: videoItemList)
//            player?.advanceToNextItem()
//            player?.seek(to: kCMTimeZero)
         //   player?.play()
      

//        }
    /*    (playerController.player as! AVQueuePlayer).removeAllItems()
        playerController.removeFromParentViewController()
        playerController = nil
        viewDidLoad()*/
        player.removeAllItems()
        player = nil
        viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let path = productCollectionList[indexPath.row] as! NSDictionary
        let carousalImage = path["images"] as! NSArray
        let imageThumb = carousalImage[0] as! NSDictionary
        let imageUrl = kBaseUrl + (imageThumb["thumb"] as! String)
        let imageView: UIImageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: cell.contentView.frame.width, height: cell.contentView.frame.height))
        imageView.tag = 20
        imageView.kf.setImage(with: URL(string: imageUrl))
        cell.contentView.addSubview(imageView)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let prev = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: prev)
        {
             cell.layer.borderWidth = 0.0
       
         //   (cell.viewWithTag(20) as! UIImageView).transform = .identity
        }
        if let next = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: next)
        {
            data = productData.object(forKey: "dataa") as! NSArray
            let carousal = data[i] as! NSDictionary
            let individualcarousel = carousal.object(forKey: "carousels") as! NSArray
            let carousalid = individualcarousel[0] as! NSDictionary
            let logo = carousalid["carouselLogo"] as! String
            ImageLoader.sharedLoader.imageForUrl(urlString: logo, completionHandler: {(image,url)in
                self.imageLogo.image = image
            })
            let path = productCollectionList[next.row] as! NSDictionary
            producttitle.text = path["title"] as? String
            pricelbl.text = "Price:" +  "  " +  "$" + "\(path["price"] as! String)"
          //  producttitle.frame = CGRect(x: 85, y: 120, width: (producttitle.text?.characters.count)!, height: 50)
              cell.layer.borderWidth = 7.0
      //      (cell.viewWithTag(20) as! UIImageView).adjustsImageWhenAncestorFocused = true
          //  (cell.viewWithTag(20) as! UIImageView).adjustsImageWhenAncestorFocused = true
//            (cell.viewWithTag(next.row) as! UIImageView).layer.shadowColor = UIColor.darkGray.cgColor
//            (cell.viewWithTag(next.row) as! UIImageView).layer.shadowRadius = 7.0
             cell.layer.borderColor = UIColor.red.cgColor
        }
        
        if context.focusHeading == .down
        {
            collectionView.isHidden = true
            quickBuyView.isHidden = true
        }
//        if context.focusHeading == .up
//        {
//            
//        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let Path = productCollectionList[indexPath.row] as! NSDictionary
        let carousalImage = Path["images"] as! NSArray
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let detailpage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
        detailpage.productlist = carousalImage
        self.navigationController?.pushViewController(detailpage, animated: true)
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        
        if context.nextFocusedView == buyButton
        {
          /*  DispatchQueue.main.async 
             {
                self.playerLayer.frame = CGRect(x: 0, y: 0, width: 1920, height: 1080)
                
            }*/
        }
        else
        {
          /*  DispatchQueue.main.async {
                self.playerLayer.frame = CGRect(x: 150, y: 0, width: 1620, height: 650)
                
            }*/
        }
    }
    func getProducts()
    {
        data = productData.object(forKey: "dataa") as! NSArray
        for carousal in data
        {
            let playerItem = AVPlayerItem(url: URL(string: ((carousal as! NSDictionary)["videoURL"] as! String))!)
            videoItemList.append(playerItem)
        }
        print(videoItemList)
        videoItemList.removeLast()
    
        
   //     Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.changeProducts), userInfo: nil, repeats: true)
        let carousal = data[i] as! NSDictionary
        let individualcarousel = carousal.object(forKey: "carousels") as! NSArray
        let carousalid = individualcarousel[0] as! NSDictionary
        productList = carousalid["products"] as! NSArray
            for individualProduct in productList
        {
            let dict = individualProduct as! NSDictionary
            productCollectionList.add(dict)
        }
    }

    func changeProducts()
    {
        productCollectionList.removeAllObjects()
        i = i+1
        let data = productData.object(forKey: "dataa") as! NSArray
        if  i <= (data.count-1)
        {
            let carousal = data[i] as! NSDictionary
            let individualcarousel = carousal.object(forKey: "carousels") as! NSArray
            let carousalid = individualcarousel[0] as! NSDictionary
            productList = carousalid["products"] as! NSArray
            for individualProduct in productList
            {
                let dict = individualProduct as! NSDictionary
                productCollectionList.add(dict)
            }
        }
        collectionView.reloadData()
    }

}

//
//  DDSApiManager.swift
//  DameDashStore
//
//  Created by Sanchan on 19/12/16.
//  Copyright © 2016 Sanchan. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class DDSApiManager
{
    static let sharedManager:DDSApiManager = DDSApiManager()
    
    func postDataWithJson(url:String,parameters:[String:[String:AnyObject]],completion:@escaping (_ responseDict:Any?,_ error:Error?,_ isDone:Bool)-> Void)
    {
        Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { response in
            do
            {
                let post:Any = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                completion(post,nil, true)
            }
            catch{
                completion(nil,response.result.error!,false)
            }
    }
   }
    
    func getJSONData(url:String,completion:@escaping (_ responseDict:Any?)-> Void)
    {
        Alamofire.request(url).responseJSON{ response in
          if response.response != nil
          {
              if response.response?.statusCode == 200
              {
                completion(response.result.value as! NSDictionary)
              }
          }
            else
          {
                completion(nil)
          }
    }
}

}

//
//  AppDelegate.swift
//  DameDashStore
//
//  Created by Sanchan on 17/12/16.
//  Copyright © 2016 Sanchan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var deviceId,uuid,userId,status:String!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let activityView = ActivityView.init(frame: (self.window?.frame)!)
        self.window?.addSubview(activityView)
        var parameters = [String:[String:AnyObject]]()
        let udid = UIDevice.current.identifierForVendor?.uuidString
        parameters = ["getActivationCode":["model":"AppleTV4Gen" as AnyObject, "manufacturer": "Apple" as AnyObject, "device_name": "AppleTV" as AnyObject, "device_id": udid! as AnyObject, "device_mac_address": "mac" as AnyObject, "brand_name": "Apple" as AnyObject, "host_name": "app" as AnyObject, "display_name": "apple" as AnyObject, "serial_number": "1234" as AnyObject]]
        
            DDSApiManager.sharedManager.postDataWithJson(url: kActivationCodeUrl , parameters: parameters) {(responseDict, error,isDone)in
            if error == nil
            {
                let dict = responseDict as! [String:String]
                self.uuid = dict["uuid"]! as String
                self.deviceId = dict["device_id"]! as String
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                if !((dict["pair_devce"]! as String).contains("active"))
                {
                    let rootView = storyBoard.instantiateViewController(withIdentifier: "Code") as! CodeViewController
                    rootView.uuid = self.uuid
                    rootView.deviceId = self.deviceId
                    rootView.code = dict["code"]
                    let navigationController = UINavigationController(rootViewController: rootView)
                    self.window?.rootViewController = navigationController
                    self.window?.makeKeyAndVisible()
                }
                
                else
                {
                    if (UserDefaults.standard.object(forKey: "_id") != nil)
                    {
                        self.userId = UserDefaults.standard.object(forKey: "_id") as! String
                        self.gotoMenu(userid: self.userId)
                    }
                }
            }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                })
                alertview.addAction(defaultAction)
                self.window?.rootViewController?.present(alertview, animated: true, completion: nil)
            }
            
            activityView.removeFromSuperview()    
        }
        return true
    }
    
    func gotoMenu(userid:String)
    {
        DDSApiManager.sharedManager.getJSONData(url: kProductUrl){
            (responseDict) in
            if responseDict != nil
            {
                let post = responseDict as! NSDictionary
                self.gotoStore(userId:userid,response:post)
            }
        }
    }
    
    func gotoStore(userId: String,response:NSDictionary)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let rootView = storyBoard.instantiateViewController(withIdentifier: "Store") as! StoreViewController
        rootView.productData = response
        rootView.userId = userId
        let navigationController = UINavigationController(rootViewController: rootView)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


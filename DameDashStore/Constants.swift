//
//  Constants.swift
//  DameDashStore
//
//  Created by Sanchan on 19/12/16.
//  Copyright © 2016 Sanchan. All rights reserved.
//

import Foundation

let kActivationCodeUrl = "http://devstore.damedashstudios.com/getActivationCode"
let kAccountInfoUrl = "http://devstore.damedashstudios.com/getAccountInfo"
let kProductUrl = "http://devstore.damedashstudios.com/gets3Files"
let kBaseUrl = "http://s3.amazonaws.com/peafowl/damedash-devstore"


var gUserID = String()

//
//  CodeViewController.swift
//  DameDashStore
//
//  Created by Sanchan on 19/12/16.
//  Copyright © 2016 Sanchan. All rights reserved.
//

import UIKit

class CodeViewController: UIViewController {

    var uuid,deviceId,userId,code: String!
    var datadict = NSDictionary()
    var timer = Timer()
    weak var codeLbl: UILabel!
    @IBOutlet weak var CodeLabel: UILabel!
    @IBOutlet weak var Codeview: UIView!
    @IBOutlet weak var clickbtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let str = "To activate go to\n\n dev.damedashstore.com/activate"
        let attributedString = NSMutableAttributedString.init(string: str)
        attributedString.addAttribute(NSFontAttributeName, value:self.CodeLabel.font, range:NSRange(location: 0, length: str.characters.count))
        attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 44), range: NSRange(location:20,length:("dev.damedashstore.com/activate" as String).characters.count))
        self.CodeLabel.attributedText = attributedString
        Codeview.layer.borderWidth = 7.0
        Codeview.layer.borderColor = UIColor.white.cgColor
        codeLbl.text = code
        clickbtn.layer.cornerRadius = 7.0
        clickbtn.backgroundColor = UIColor.init(red: 179/255, green: 22/255, blue: 28/255, alpha: 1)
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.accountstatus), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }

    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        if context.nextFocusedView == clickbtn
        {
            clickbtn.setTitleColor(UIColor.white, for: .focused)
            clickbtn.backgroundColor = UIColor.init(red: 179/255, green: 22/255, blue: 28/255, alpha: 1)
        }
    }
    
    func accountstatus()
    {
        let parameters = ["getAccountInfo": ["deviceId": deviceId!, "uuid": uuid!]]
        print(parameters)
        DDSApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters as [String : [String : AnyObject]])
        {
            (responseDict,error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let dict = JSON as! NSDictionary
                self.datadict = dict
                if dict.allValues.isEmpty
                {
                    
                }
                else if (dict["user_status"] != nil)
                {
                    if (((dict["user_status"]!) as AnyObject).contains("active"))
                    {
                        self.userId = dict["_id"] as! String
                        gUserID = dict["_id"] as! String
                        UserDefaults.standard.set(gUserID, forKey: "_id")
                        UserDefaults.standard.synchronize()
                        self.timer.invalidate()
                        self.gotoRoot()
                    }
                }
            }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                })
                alertview.addAction(defaultAction)
                self.navigationController?.pushViewController(alertview, animated: true)
            }
        }
    }
    func gotoRoot()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.gotoMenu(userid: self.userId)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

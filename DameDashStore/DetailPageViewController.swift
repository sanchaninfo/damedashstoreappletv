//
//  DetailPageViewController.swift
//  DameDashStore
//
//  Created by Sanchan on 06/01/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class DetailPageViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var productlist = NSArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productlist.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        for i in 0..<productlist.count
        {
            let image1 = productlist[i] as! NSDictionary
            let image = kBaseUrl + (image1["thumb"] as! String)
            ImageLoader.sharedLoader.imageForUrl(urlString: image, completionHandler: {(image, url) in
                (cell.viewWithTag(10) as! UIImageView).image = image
            })
        }
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
